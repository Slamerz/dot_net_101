# .NET 101

Welcome to the Kenzie Academy .NET/C# learning group!



Contents:
## Getting Started
> [What is C# and .NET?](./Getting_Started/what_is_dotnet.md)

> [Setting up](./Getting_Started/setting_up.md)

> [Hello World](./Getting_Started/hello_world.md)

