# What is C# and .NET
A basic way to think about it is C# is a strongly typed programing language that is toted for being easy to learn, hard to master thanks to its typing system, and .NET is an API. (Coming from something like Javascript think of .NET as a large package with many tools to help you create desktop and web applications.)

## C# 
* C# is intended to be a simple, modern, general-purpose, object-oriented programming language.
* C# and implementations thereof, should provide support for software engineering principles such as strong type checking, array bounds checking, detection of attempts to use uninitialized variables, and automatic garbage collection. Software robustness, durability, and programmer productivity are important.
* C# is intended for use in developing software components suitable for deployment in distributed environments.
* Portability is very important for source code and programmers, especially those already familiar with C and C++.
* Support for internationalization is very important.
* C# is intended to be suitable for writing applications for both hosted and embedded systems, ranging from the very large that use sophisticated operating systems, down to the very small having dedicated functions.
* Although C# applications are intended to be economical with regard to memory and processing power requirements, the language was not intended to compete directly on performance and size with C or assembly language.

## .NET
* .NET is a free, cross-platform, open source developer platform for building many different types of applications.

* With .NET, you can use multiple languages, editors, and libraries to build for web, mobile, desktop, gaming, and IoT.

.NET is actually broken into three very distinct versions, each with their own uses.
#### .NET Core 
a cross-platform .NET implementation for websites, servers, and console apps on macOS, Windows, and Linux.

#### .NET Framework 
supports websites, services, desktop apps, and more on Windows.

#### Xamarin/Mono 
a .NET implementation for running apps on all the major mobile operating systems.

### Links:
[What is .Net](https://dotnet.microsoft.com/learn/dotnet/what-is-dotnet)\
[.NET Languages](https://dotnet.microsoft.com/languages)
