# C# Hello World

Now that you have .NET Core installed and your visual studio code setup to work in C# it's time we make our first project!

C# is what is called a compiled language, meaning that in order to run the code we first have to compile it into a program and then run said program.
Because of this there are quite a few files and folders we need in our project to tell our compiler what we want to build, and how to build it. Lucky for us .NET comes with many templates to help start your projects to make sure you've got what you need right off the bat.\
Types of Templates:
* console
* angular
* react
* reactredux
* razorclasslib
* classlib
* mstest
* xunit
* nunit
* page
* web
* mvc
* webapp
* webapi
* globaljson

Most types of project you would want to start there is already a built-in template for, so lets start with a console application.\
To create a .NET project we'll use the `dotnet new` command, so we go into our terminal and navigate to the desired location we want to create our application/project and we run\
`dotnet new console`\
Using `new` lets dotnet know to create a project, and then we tell it the template to use, in this case `console`. This creates an app that will by default take in the name of whatever folder it was created in, and create a c# project.
This creates a brand new "Hello World" console based application for us, giving us an `obj` folder, a `.csproj` file named after the parent folder, and a `Program.cs` file and if we run the project with `dotnet run` we should see "Hello World!" in the console, but if we look at our folder now we will have a new folder named `bin`.\
For more information on option flags, and the `dotnet new` command check out this [Link](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new?tabs=netcore22)

Now lets walk through each of these files/folders and discuss their purpose.
### Project.cs
```c#
using System;

namespace example
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
```
This is where our actual C# lives, and when we go to add code this is where we'll start.

### *.csproj
```html
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.0</TargetFramework>
  </PropertyGroup>

</Project>
```
This file tells the project what we want to build when we run. By default this one already tells our project what version of .Net Core we'd like to build this with `netcoreapp3.0` as well as what type of executable to create when we build it `.exe` file.


### obj folder
This folder stores a lot of our configuration files, and stores our packages. At this phase you won't need to worry about manually changing any of these files, thanks to tools like nuget when we add packages this folder will automatically be handled for us.

### bin folder
Whenever we run the program it actually has to compile the project into a full program, and the bin folder is where it stores said program.\
While build times on larger projects can be a bit of a hassle every time you'd like to test a feature the compiled version of the code runs extremely quickly, so we sacrifice build time, once it's built its run time is considerably faster than say running a javascript project of the same size.\
This also helps because if say you wanted to take this program and run it on a different machine you can take the build folder from the `bin` and put it on another machine and simply run the `.exe`. All the needed files and dependencies are stored, so no need to run something like `npm install` before trying to run the program on other systems.

