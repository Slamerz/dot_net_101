# Settings Up
As many Kenzie students are not running windows systems then to keep everyone on the same page we'll start by working in .NET Core, a cross platform SDK that is compatible with Linux, Windows, and MacOS.

1. we'll need to install .Net Core to start and compile our applications
[Download .NET Core](https://dotnet.microsoft.com/download)
2. If you haven't already, install visual studio Code [Download VS Code](https://code.visualstudio.com/)
    * Install c# extension for VS code [Download VS Code Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
    
And bam you're all ready to create your first .NET Core Application!



